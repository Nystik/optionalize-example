enum Tag {
  Paypal = 'paypal',
  Steam = 'steam',
  Itchio = 'itch.io',
  Humble = 'humble bundle',
  Gog = 'gog',
}

interface TransactionInfo {
  accountingDate: string
  chargeDate: string
  verificationNumber: string
  text: string
  amount: number
}

interface Category {
  name: string
  tags: Tag[]
  path?: string
}

interface ExtendedTransactionInfo {
  receipt?: string
  product?: string
}

interface TaggedTransaction {
  info: TransactionInfo
  tag?: Tag
  extraTags: Tag[]
  extendedInfo?: ExtendedTransactionInfo
}

export type {
  Tag,
  TransactionInfo,
  Category,
  ExtendedTransactionInfo,
  TaggedTransaction,
}
