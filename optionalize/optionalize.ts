import type { Option } from '@wentools/option'
import type { Intersection } from './intersect'

/** Pick optional properties from T */
type OptionalPropertiesOf<T extends object> = Pick<
  T,
  Exclude<
    {
      [K in keyof T]: T extends Record<K, T[K]> ? never : K
    }[keyof T],
    undefined
  >
>

/** Pick required properties from T */
type RequiredPropertiesOf<T extends object> = Pick<
  T,
  Exclude<
    {
      [K in keyof T]: T extends Record<K, T[K]> ? K : never
    }[keyof T],
    undefined
  >
>

/** Wrap optional properties in Option and make required */
type Optionalize<Type extends Object> = Intersection<
  RequiredPropertiesOf<Type>,
  {
    [Property in keyof OptionalPropertiesOf<Type>]-?: Option<
      NonNullable<Type[Property]>
    >
  }
>

export type { Optionalize }
