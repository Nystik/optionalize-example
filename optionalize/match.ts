/** Returns the keys of type SomeInterface */
type Matching<T, SomeInterface> = {
  [K in keyof T]: T[K] extends SomeInterface ? K : never
}[keyof T]

/** Returns the keys not of type SomeInterface */
type NonMatching<T, SomeInterface> = {
  [K in keyof T]: T[K] extends SomeInterface ? never : K
}[keyof T]

export type { Matching, NonMatching }
