import { none, some } from '@wentools/option'
import * as Mapped from './mapped-types'

const t: Mapped.TaggedTransaction = {
  info: {
    accountingDate: '',
    chargeDate: '',
    verificationNumber: '',
    text: '',
    amount: 0,
  },
  tag: none(),
  extraTags: [],
  extendedInfo: some({
    receipt: none(),
    product: some('groceries'),
  }),
}
